import React from 'react';
import "./ColorModal.css";
const colors=["red", "blue", "green", "brown", "cadetblue", "orange", "grey", "steelblue", "pink"];
const ColorModal = (props)=>{
    return(
    <div className="colorModal">
      {colors.map(d=><button key={d} style={{backgroundColor:d}} onClick={()=>{props.setColor(d);}}>{d}</button>)}
    </div>
    );
}
export default ColorModal;