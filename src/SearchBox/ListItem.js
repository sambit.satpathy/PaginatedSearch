import React from 'react';
import "./ListItem.css";
const ListItem = (props)=>{
  let text =[];
  props.text.split(" ").forEach((d, i)=>{
    let flag=false;
    for(let _key in props.tags){
      if(props.tags[_key].exp.test(d)){
        text[i]=(<span
                  key={_key}
                  style={{backgroundColor:props.tags[_key].color}}
                  className={props.tags[_key].color}
                 >{d}</span>);;
        flag=true;
      }
    }
    if(!flag){
      text.push(d);
    }
  });
    return(
    <div className="listItem">
      <span onMouseUp={props.onSelectEnd}>
        {text}
      </span>
      <div className="buttonGroup">
        <button onClick={props.setLabel}>Use as label</button>
        <button onClick={props.deleteItem}>x</button>
      </div>
    </div>
    );
}
export default ListItem;