import React, { Component } from 'react';
import ListItem from "./ListItem";
import ColorModal from "./ColorModal";
const pageSize=2;

class PaginatedList extends Component {
 constructor(props){
     super(props);
     this.state={
         activePage:1,
         pageCount:Math.ceil(props.list.length/pageSize),
         tags:{},
         showModal:false
     }
     this.navButtons=this.navButtons.bind(this);
     this.setActivePage=this.setActivePage.bind(this);
     this.selectTextEnd=this.selectTextEnd.bind(this);
     this.setColor=this.setColor.bind(this);
 }
 componentWillReceiveProps(nextProps){
     this.setState({pageCount:Math.ceil(nextProps.list.length/pageSize)})
 }
 setColor(color){
   let tags = {};
   this.state.showModal.split(" ").forEach(d=>{
       if(d.trim()!==""){
        tags[d]={color:color, exp:RegExp(".*"+d+".*","gi")};
       }
   });
   this.setState({
    tags:{...this.state.tags, ...tags},
    showModal:false
   });
 }

selectTextEnd(e){
   let tag= e.target.textContent.substring(document.getSelection().focusOffset, document.getSelection().baseOffset);
   this.setState({showModal:tag});
 }
 navButtons(){
    let buttons=[],
    content="",
    id=0,
    i=0;
    while(i<=this.state.pageCount+1){
        if(i===0){
            content="<";
            id=1;
        }else if(i===this.state.pageCount+1){
            content=">";
            id=this.state.pageCount;
        }else{
            content=i;
            id=i;
        }
        buttons.push(<button
                        key={i}
                        data-id={id}
                        disabled={this.state.activePage===id}
                        onClick={this.setActivePage}
                     >{content}</button>);
        i++;
    }
    return buttons;
 }
 setActivePage(e){
    this.setState({activePage:+e.target.getAttribute("data-id")});
 }
 render(){
    let displayPage=[],
    last=((this.state.activePage-1) *pageSize),
    i=last;
    for(i; i<last+pageSize && i<this.props.list.length; i++){
        displayPage.push(this.props.list[i]);
    }
    return(
      <section>
        {this.state.showModal && <ColorModal setColor={this.setColor}/>}
        <ul>
        {displayPage.map(d=><ListItem
                                 key={d.id}
                                 setLabel={this.props.setLabel.bind(this, d.text)}
                                 deleteItem={this.props.deleteItem.bind(this, d.id)}
                                 text={d.text}
                                 onSelectEnd={this.selectTextEnd}
                                 tags={this.state.tags}
                                />)}
        </ul>
        <footer>
            {this.navButtons()}
        </footer>
      </section>
    );
 }
}
export default PaginatedList;