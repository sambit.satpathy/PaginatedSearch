import React, { Component } from 'react';
import PaginatedList from "./PaginatedList";

class SearchBox extends Component {
 constructor(props){
    super(props);
    this.state={
        filteredList:props.dataList,
        selectedLabel:""
    }
    this.setLabel=this.setLabel.bind(this);
 }
 componentWillReceiveProps(nextProps){
    this.filterList(nextProps.dataList);
 }
 setLabel(text){
  this.setState({selectedLabel:text});
 }
 deleteFromList(item){

 }
 filterList(filteredList, e){
    if(e){
      let searchPattern = RegExp(e.target.value,"gi");
      filteredList = filteredList.filter(d=>searchPattern.test(d.text));
    }
    this.setState({filteredList});
 }
 render(){
    return(
    <div>
      {this.state.selectedLabel && <label>{`Label: ${this.state.selectedLabel}`}</label>}
      <input type="search"
        placeholder="Search"
        onChange={(e)=>{this.filterList(this.props.dataList, e);}}
      />
      <PaginatedList
        list={this.state.filteredList}
        setLabel={this.setLabel}
        deleteItem={this.props.deleteItem}
      />
    </div>
    );
 }
}
export default SearchBox;