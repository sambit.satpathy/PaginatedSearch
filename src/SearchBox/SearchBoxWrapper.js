import React, { Component } from 'react';
import SearchBox from "./SearchBox";

class SearchBoxWrapper extends Component {
  constructor(props){
      super(props);
      this.state={
          dataList:[]
      }
      this.onKeyPress=this.onKeyPress.bind(this);
      this.deleteItem=this.deleteItem.bind(this);
  }
  onKeyPress(e){
      if(e.key==="Enter"){
        let {dataList}=this.state;
        dataList.push({text:e.target.value, id:dataList.length});
        this.setState(dataList);
        e.target.value="";
      }
  }
  deleteItem(id){
    let {dataList}=this.state;
    dataList=dataList.filter(d=>d.id!==id);
    this.setState({dataList});
  }
  render() {
    return (
      <div>
        <h2>User Says</h2>
        <input
          type="text" onKeyPress={ this.onKeyPress }
          placeholder="Add what user will say and press <Enter>"
        />
        <SearchBox dataList={this.state.dataList} deleteItem={this.deleteItem}/>
      </div>
    );
  }
}
export default SearchBoxWrapper;
