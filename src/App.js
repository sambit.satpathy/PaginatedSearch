import React, { Component } from 'react';
import './App.css';
import SeachBoxWrapper from "./SearchBox/SearchBoxWrapper";

class App extends Component {
  render() {
    return (
      <div className="App">
        <SeachBoxWrapper/>
      </div>
    );
  }
}

export default App;
